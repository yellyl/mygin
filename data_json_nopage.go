/*

基于 context 的扩展方法

处理了：
	1. 自定义JSON输出格式；
	2. 常用的翻页处理；


2021/6/7

*/

package mygin

import "time"

// DataNoPageResponse data struct
type DataNoPageResponse struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Time int         `json:"time"`
	Data interface{} `json:"data"`
}

// ServerDataNoPageJSON response JSON format
//
//	statusCode httpStatusCode
//	like:  c.ServerDataNoPageJSON(401,1,"UnAuthorized")
func (c *Context) ServerDataNoPageJSON(statusCode int, args ...interface{}) {
	var (
		err  error
		data interface{}
		msg  string
		code int
	)
	for _, v := range args {
		switch vv := v.(type) {
		case int:
			code = vv
		case string:
			msg = vv
		case error:
			err = vv
		default:
			data = vv
		}
	}
	if err != nil {
		debugPrint("[error]", c.Request.URL.String(), err.Error())
	}
	if code == 0 && msg == "" {
		msg = "success"
	}

	resp := &DataNoPageResponse{
		Code: code,
		Msg:  msg,
		Time: int(time.Now().Unix()),
		Data: data,
	}
	c.ServerJSON(statusCode, &resp)
	return
}

// DataNoPageJSON response JSON format
//
//	c.ServerDataJSON(1,"lost param")
//	c.ServerDataJSON()
func (c *Context) DataNoPageJSON(args ...interface{}) {
	c.ServerDataNoPageJSON(200, args...)
}
